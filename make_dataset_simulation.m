clear all, close all

% let's create a labelled dataset. 

order_max               = 5;
N_points_frf            = 100;
N_elements_in_dataset   = 10000;
tic
for iter_elem_data = 1:N_elements_in_dataset
    polynomial_orders       = sort(randi(order_max+1, [2 1])); % sort 2 random integers in ascending order. The first one indicates the number of zeros, the second the number of poles. use -1 to allow for no zeros/poles
    if all(polynomial_orders == [1;1]); polynomial_orders = [1; 2]; end % in case we get [1 1] (static system), override to get a first order. This means we get more 1st order that the others. 
    
    b           = rand(1, polynomial_orders(1));
    a           = rand(1, polynomial_orders(2)); % only positive coefficients from 0 to 1. Some systems are thus unstable ... 
    [frf, w]    = freqs(b, a, N_points_frf);
    
    dataset{iter_elem_data} = {polynomial_orders, frf}; % the labels are the first field, the data (the frf), the second. Angular frequency is omitted for the moment from the dataset.
end
toc
%plot(log10(w), db(abs(frf))); % to look at the frf of the last system

% the labels are vectors of 2 integers. find the amount of possibilities. the number of poles should always be greater
% or equal than the number of zeros to have a proper (causal) system
order_a = 1:order_max+1; 
order_a = repmat(order_a', order_max+1, 1);
order_b = 1:order_max+1;
order_b = reshape(repmat(order_b, order_max+1, 1), [], 1);
indicies_proper_system  = order_a >= order_b;
labels_possible         = [order_b(indicies_proper_system) order_a(indicies_proper_system)];
% remove the static system [1 1]
labels_possible         = labels_possible(2:end, :);
number_of_labels        = size(labels_possible, 1);

% how many of each label do we have in the generated dataset ? 
tic
labels = zeros(N_elements_in_dataset, number_of_labels);
for iter_elem_data = 1:N_elements_in_dataset
    try
    labels(iter_elem_data, :) = all(~(labels_possible - dataset{iter_elem_data}{1}')');
    catch
        oi = 1;
    end
end
toc
number_of_labels_in_each_class = sum(labels) % because of the sort operation, some model orders get more representation in the dataset population

assert(number_of_labels_in_each_class == N_elements_in_dataset); % check that all labels are accounted for.
% we generated 10000 points, spread in (order_max+1)^2 classes. Due to the sorting, when the 2 numbers are different, we
% get 2* the representation. For the 1st order class [1 2] we get 3* the representation since we also have the
% degenerate case of the static system of order [1 1]